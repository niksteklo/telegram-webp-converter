<?php

namespace Root\TelegramWebpConverter\Models;

use GuzzleHttp\Client;
use Root\TelegramWebpConverter\DTO\Message;
use Root\TelegramWebpConverter\DTO\MethodUrlDto;
use Root\TelegramWebpConverter\DTO\NewMessageDto;
use Root\TelegramWebpConverter\DTO\NewPhotoMessageDto;
use Root\TelegramWebpConverter\Helpers\Env;
use Root\TelegramWebpConverter\TelegramMessageMethodsEnum;

class Telegram
{
    private Client $client;

    public ?Message $lastMessage = null;

    private int $offset = 0;

    public function __construct()
    {
        $this->client = new Client([
            'base_uri' => 'https://api.telegram.org',
        ]);
    }

    public function getUpdates(): null|Message
    {
        // TODO: переписать на вебхук
//        https://core.telegram.org/bots/api#getupdates
        $response = $this->client->get(
            $this->getMethodUrl(new MethodUrlDto(TelegramMessageMethodsEnum::GET_UPDATES)),
            $this->prepareGetData([
                'chat_id' => Env::get('CHAT_ID'),
                'offset' => $this->offset,
            ])
        );

        $messages = json_decode($response->getBody()->getContents(), true)['result'];

        if (!isset($messages[0]['message']['photo'])) {
            $this->lastMessage = null;

            if (isset($messages[0]['update_id'])) {
                $this->offset = $messages[0]['update_id'] + 1;
            }

            return null;
        }

        $message = $messages[0];

        $pic = end($message['message']['photo']);

        $lastMessage = new Message(
            $message['update_id'],
            $message['message']['message_id'],
            $message['message']['from']['username'],
            $message['message']['text'] ?? null,
            $this->getFile($pic['file_id']),
        );

        $this->lastMessage = $lastMessage;
        $this->offset = $lastMessage->update_id + 1;

        return $lastMessage;
    }

    // TODO: Добавить обработку ошибок и проверку ответов сервера
    public function sendMessage(NewMessageDto $dto): void
    {
        $this->client->post(
            $this->getMethodUrl(new MethodUrlDto(TelegramMessageMethodsEnum::SEND_MESSAGE)),
            $this->preparePostData([
                'chat_id' => $dto->chatId,
                'text' => $dto->text,
                'reply_to_message_id' => $dto->replyMessage?->message_id,
            ])
        );
    }

    // TODO: Добавить обработку ошибок и проверку ответов сервера
    public function sendPhoto(NewPhotoMessageDto $dto): void
    {
        $this->client->post(
            $this->getMethodUrl(new MethodUrlDto(TelegramMessageMethodsEnum::SEND_PHOTO)),
            $this->prepareMultiformData([
                'chat_id' => $dto->chatId,
                'photo' => fopen($dto->photoPath, 'r'),
                'reply_to_message_id' => $dto->replyMessage?->message_id,
            ]),
        );
    }

    private function getFile(string $fileId): string
    {
        $fileDataResponse = $this->client->get(
            $this->getMethodUrl((new MethodUrlDto(TelegramMessageMethodsEnum::GET_FILE))),
            $this->prepareGetData([
                'file_id' => $fileId,
            ])
        );

        $fileInfoData = json_decode($fileDataResponse->getBody()->getContents(), true);

        $filePath = $fileInfoData['result']['file_path'];

        $photo = $this->client->get(
            $this->getMethodUrl(new MethodUrlDto(TelegramMessageMethodsEnum::FILE, $filePath))
        );

        $savedFilePath = sprintf('download/%s.%s', time(), 'jpg');

        file_put_contents($savedFilePath, $photo->getBody());

        return $savedFilePath;
    }

    private function getMethodUrl(MethodUrlDto $dto): string
    {
        return match ($dto->method) {
            TelegramMessageMethodsEnum::FILE => sprintf('/file/bot%s/%s', Env::get('TOKEN'), $dto->filePath),
            default => sprintf('/bot%s/%s', Env::get('TOKEN'), $dto->method->value),
        };
    }

    private function preparePostData(array $data): array
    {
        return ['json' => $data];
    }

    private function prepareGetData(array $data): array
    {
        return ['query' => $data];
    }

    private function prepareMultiformData(array $data): array
    {
        $inner = [];

        foreach ($data as $key => $value) {
            $inner[] = [
                'name'     => $key,
                'contents' => $value,
            ];
        }

        return ['multipart' => $inner];
    }
}