<?php

namespace Root\TelegramWebpConverter\Helpers;

class Env
{
    public static function get(string $name): mixed
    {
        return $_ENV[$name] ?? null;
    }
}
