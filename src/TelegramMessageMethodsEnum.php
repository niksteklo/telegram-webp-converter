<?php

namespace Root\TelegramWebpConverter;

enum TelegramMessageMethodsEnum: string
{
    case SEND_MESSAGE = 'sendMessage';

    case SEND_PHOTO = 'sendPhoto';

    case GET_UPDATES = 'getUpdates';

    case GET_FILE = 'getFile';

    case FILE = 'file';
}
