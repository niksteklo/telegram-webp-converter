<?php

namespace Root\TelegramWebpConverter\DTO;

class NewPhotoMessageDto
{
    public function __construct(
        public readonly int $chatId,
        public readonly string $photoPath,
        public readonly null|Message $replyMessage = null,
    ){}
}
