<?php

namespace Root\TelegramWebpConverter\DTO;

class NewMessageDto
{
    public function __construct(
        public readonly int $chatId,
        public readonly string $text,
        public readonly null|Message $replyMessage = null,
    ){}
}
