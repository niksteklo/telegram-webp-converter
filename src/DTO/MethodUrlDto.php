<?php

namespace Root\TelegramWebpConverter\DTO;

use Root\TelegramWebpConverter\TelegramMessageMethodsEnum;

class MethodUrlDto
{
    public function __construct(
        public readonly TelegramMessageMethodsEnum $method,
        public null|string $filePath = null,
    ){}
}
