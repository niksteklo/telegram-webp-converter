<?php

namespace Root\TelegramWebpConverter\DTO;

class Message
{
    public function __construct(
        public readonly string $update_id,
        public readonly string $message_id,
        // TODO: Если нужно будет - добавить больше инфы о сообщении
        public readonly string $username,
        public readonly null|string $text = null,
        public readonly null|string $photo = null
    ){}
}
