<?php

require_once 'vendor/autoload.php';

use Root\TelegramWebpConverter\DTO\NewPhotoMessageDto;
use Root\TelegramWebpConverter\Helpers\Env;
use Root\TelegramWebpConverter\Models\Telegram;
use Symfony\Component\Dotenv\Dotenv;

$dotenv = new Dotenv();
$dotenv->load(__DIR__.'/.env');

$telegramSdk = new Telegram();

main($telegramSdk);

function main(Telegram $telegramSdk): void
{
    if ($lastMessage = $telegramSdk->getUpdates()) {
        // TODO: Добавить удаление фото и конвертации в webp
        $telegramSdk->sendPhoto(new NewPhotoMessageDto(
            Env::get('CHAT_ID'),
            $lastMessage->photo,
            $lastMessage,
        ));
    }

    sleep(1);

    main($telegramSdk);
}